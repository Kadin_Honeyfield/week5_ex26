﻿using System;

namespace week5_ex26
{
    class Program
    {
        static void Main(string[] args)
        {
            var b1 = new BankAccount(30.20 );//Sets b1 to 30.20
            var b2 = new BankAccount(1004.69 );//Sets b1 to 30.20
            Console.WriteLine(b1.ShowBalance);//takes 30.20 and runs it through ShowBalance
            Console.WriteLine(b2.ShowBalance);

        }
        private class BankAccount//Bank account class
        {
            private double Balance;//declares Balance as a double

            public BankAccount (double _balance)//BankAccount contructor
            {
                Balance = _balance;//sets Balance to _balance
            }

            public string ShowBalance//Creates a property
            {
                get //getter
                {
                    var a = $"{Balance:C2}";//Sets "a" to {Balance:C2}
                    return a;//returns "a" to Balance(Which is now "Balance:C2")
                }
            }

            
        }
    }
}
